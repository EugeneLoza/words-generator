unit UnitTestWords;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  CastleWordsGenerator;

type
  TForm1 = class(TForm)
    ButtonGenerate: TButton;
    ComboBoxDictionary: TComboBox;
    MemoNames: TMemo;
    procedure ButtonGenerateClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.ButtonGenerateClick(Sender: TObject);
var
  W: TCastleWordsGenerator;
  I: Integer;
begin
  Randomize;
  StaticNamespace := TNamespace.Create;
  DynamicNamespace := TNamespace.Create;
  W := TCastleWordsGenerator.Create;
  W.Load('castle-data:/' + ComboBoxDictionary.Text + '.dic');
  MemoNames.Clear;
  for I := 0 to 100 do
    MemoNames.Lines.Add(W.Generate);
  W.Free;
  DynamicNamespace.Free;
  StaticNamespace.Free;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ComboBoxDictionary.Clear;
  ComboBoxDictionary.AddItem('chemicals', nil);
  ComboBoxDictionary.AddItem('girls', nil);
  ComboBoxDictionary.AddItem('boys', nil);
  ComboBoxDictionary.Text := ComboBoxDictionary.Items[0];
  ButtonGenerateClick(nil);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin

end;

end.

