unit CastleWordsGenerator;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, Generics.Collections,
  CastleUtils;

type
  TSyllable = record
    Value: String;
    Frequency: Integer;
  end;

type
  TSyllables = specialize TStructList<TSyllable>;

type
  TCastleWordsGenerator = class(TObject)
  strict private
    FirstSyllables: TSyllables;
    MiddleSyllables: TSyllables;
    LastSyllables: TSyllables;
  public
    procedure Load(const AUrl: String);
    function Generate: String;
    constructor Create; //override;
    destructor Destroy; override;
  end;
  //TWordsGeneratorDictionary = specialize TObjectDictionary<String, TCastleWordsGenerator>

type
  TNamespace = class(TStringList)
  public
    //SaveToXml
  end;

var
  StaticNamespace, DynamicNamespace: TNamespace;

implementation
uses
  SysUtils,
  CastleDownload, CastleStringUtils;

function TCastleWordsGenerator.Generate: String;
var
  I, N: Integer;
  A: array [0..3] of Integer;

  function GetRandomSyllableIndex(const ASyllables: TSyllables): Integer;
  begin
    repeat
      Result := Random(ASyllables.Count);
    until Random < 1 / (ASyllables[Result].Frequency + 1); // this is inefficient and not very high-quality, but guarantees we won't "accidentally" get a dead freeze because the syllables with "minimal" score cannot create a single combination that passes Namespaces check
  end;

  function Readability(const AWord: String): Boolean;
    function IsConsonant(const AChar: String): Boolean;
    const
      Consonants = ['q', 'w', 'r', 't', 'p', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'];
    var
      S: String;
    begin
      for S in Consonants do
        if AChar = S then
          Exit(true);
      Result := false;
    end;
    function IsVowel(const AChar: String): Boolean;
    const
      Vowels = ['e', 'y', 'u', 'i', 'o', 'a'];
    var
      S: String;
    begin
      for S in Vowels do
        if AChar = S then
          Exit(true);
      Result := false;
    end;
  var
    J: Integer;
    C, LastC: String;
    SequentialConsonants, SequentialVowels, SequentialLetters: Integer;
  begin
    SequentialConsonants := 0;
    SequentialVowels := 0;
    SequentialLetters := 0;
    LastC := '';
    for J := 1 to Pred(Length(AWord)) do // Utf8Length
    begin
      C := Copy(AWord, J, 1); // Utf8Copy
      if IsConsonant(C) then
      begin
        Inc(SequentialConsonants);
        SequentialVowels := 0;
      end else
      if IsVowel(C) then
      begin
        Inc(SequentialVowels);
        SequentialConsonants := 0;
      end
      else
        raise Exception.Create('Unknown letter ' + C);
      if (SequentialConsonants > 3) or (SequentialVowels >= 3) then
        Exit(false);

      if C = LastC then
        Inc(SequentialLetters)
      else
        SequentialLetters := 0;
      if SequentialLetters > 1 then
        Exit(false);
      LastC := C;
    end;
    Result := true;
  end;

begin
  repeat
    N := 1 + Random(2);
    A[0] := GetRandomSyllableIndex(FirstSyllables);
    for I := 1 to N - 1 do
      A[I] := GetRandomSyllableIndex(MiddleSyllables);
    A[N] := GetRandomSyllableIndex(LastSyllables);

    Result := FirstSyllables[A[0]].Value;
    for I := 1 to N - 1 do
      Result += MiddleSyllables[A[I]].Value;
    Result += LastSyllables[A[N]].Value;
  until (StaticNamespace.IndexOf(Result) = -1) and (DynamicNamespace.IndexOf(Result) = -1) and Readability(Result);
  DynamicNamespace.Add(Result);
  Inc(FirstSyllables.L[A[0]].Frequency);
  for I := 1 to N - 1 do
    Inc(MiddleSyllables.L[A[I]].Frequency);
  Inc(FirstSyllables.L[A[N]].Frequency);
end;

constructor TCastleWordsGenerator.Create;
begin
  inherited; // empty
  FirstSyllables := TSyllables.Create;
  MiddleSyllables := TSyllables.Create;
  LastSyllables := TSyllables.Create;
end;

destructor TCastleWordsGenerator.Destroy;
begin
  FirstSyllables.Free;
  MiddleSyllables.Free;
  LastSyllables.Free;
  inherited;
end;

procedure TCastleWordsGenerator.Load(const AUrl: String);

  procedure AddOrIncrease(const AValue: String; const ASyllables: TSyllables);
  var
    I: Integer;
    Syllable: TSyllable;
  begin
    for I := 0 to Pred(ASyllables.Count) do
      if ASyllables[I].Value = AValue then
        Exit;
    Syllable.Value := AValue;
    Syllable.Frequency := 0;
    ASyllables.Add(Syllable);
  end;

var
  Stream: TStream;
  Strings: TStringList;
  Syllables: TCastleStringList;
  S, TrimS: String;
  I: Integer;
begin
  Stream := Download(AUrl);
  Strings := TStringList.Create;
  Strings.LoadFromStream(Stream);
  Stream.Free;
  for S in Strings do
  begin
    TrimS := Trim(S);
    if TrimS <> '' then
    begin
      StaticNamespace.Add(StringReplace(TrimS, '/', '', [rfReplaceAll]));
      Syllables := CreateTokens(TrimS, ['/']);
      AddOrIncrease(Trim(Syllables[0]), FirstSyllables);
      if Syllables.Count > 1 then
      begin
        for I := 1 to Pred(Syllables.Count) - 1 do
          AddOrIncrease(Trim(Syllables[I]), MiddleSyllables);
        AddOrIncrease(Trim(Syllables[Pred(Syllables.Count)]), LastSyllables);
      end;
      Syllables.Free;
    end;
  end;
  Strings.Free;
end;

end.

